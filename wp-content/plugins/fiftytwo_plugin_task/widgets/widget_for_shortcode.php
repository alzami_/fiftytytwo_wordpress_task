<?php

function fiftytwo_load_widget() {
    register_widget( 'fiftytwo_shortcode_widget' );
}
add_action( 'widgets_init', 'fiftytwo_load_widget' );

// Creating the widget 
class fiftytwo_shortcode_widget extends WP_Widget {
  
    function __construct() {
        parent::__construct(
        
        // Base ID of your widget
        'fiftytwo_shortcode_widget', 
        
        // Widget name will appear in UI
        __('Fiftytwo Plugin Task Widget', 'fiftytwo_plugin'), 
        
        // Widget description
        array( 'description' => __( 'Sample widget for using shortcode', 'fiftytwo_plugin' ), ) 
        );
    }
      
    // Creating widget front-end
      
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $content = apply_filters( 'widget_text', $instance['content'] );

        
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
        echo $args['before_title'] . $title . $args['after_title'];
        
        // This is where you run the code and display the output
        echo $content;
        echo $args['after_widget'];
    }
              
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        if ( isset($instance['content'])){
            $content = $instance[ 'content' ];
        }
        else {
            $title = __( 'New title', 'fiftytwo_plugin' );
            $content = __('New Content', 'fiftytwo_plugin');
        }
            // Widget admin form
            ?>
            <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id( 'content' ); ?>"><?php _e( 'Content:' ); ?></label> 
            <textarea class="widefat" id="<?php echo $this->get_field_id( 'content' ); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>" ><?php echo esc_attr( $content ); ?></textarea>
            </p>
        <?php 
    }
          
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['content'] = ( ! empty( $new_instance['content'] ) ) ? strip_tags( $new_instance['content'] ) : '';

        return $instance;
    }
     
    // Class wpb_widget ends here
    } 

?>