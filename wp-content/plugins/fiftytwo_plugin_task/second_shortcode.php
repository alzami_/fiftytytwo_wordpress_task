<?php
 function rest_shortcode_handler_function( $atts, $content, $tag ){
   extract(shortcode_atts(array(
       'historical' => ''
   ), $atts));

   $latest_currency_rate;

    if($historical){
      $latest_currency_rate = file_get_contents('https://api.exchangeratesapi.io/2010-02-22');
    }else {
        $latest_currency_rate = file_get_contents('https://api.exchangeratesapi.io/latest');

    }
      $decoded_data = json_decode($latest_currency_rate);
      $rate = $decoded_data->rates;
      //print_r($rate);
      if($historical) {
          echo '<h2>Historical Data</h2>';
      }else {
        echo '<h2>Latest Data</h2>';
      }
      echo '<hr>';
      echo '<div style="color:red;font-size:30px;"> Base : '.$decoded_data->base.'<br>'.$decoded_data->date.'</div>';
      echo '<table id="rate_table">';
      echo '<thead>';
      echo '<th>Currency Name</th>';
      echo '<th>Currency Code</th>';
      echo '<th>Exchange Rate</th>';
      echo '</thead>';
      echo '<tbody>';
      foreach($rate as $key=>$value){
          echo '<tr>';
          $currency_full_name = Currency::getCurrencyFullNameByCode($key);
          echo '<td >'.$currency_full_name.'</td>';
          echo '<td>'.$key.'</td>';
          echo '<td>'.$value.'</td>';
          echo '</tr>';
      }
      echo '</tbody>';
      echo '</table>';

     // return '<h1>hello Rest</h1>';

 }


 add_action('init', 'rest_shortcode_init');


 function rest_shortcode_init(){

   add_shortcode( 'fiftytwo_shortcode_three', 'rest_shortcode_handler_function' );

 }


 ?>