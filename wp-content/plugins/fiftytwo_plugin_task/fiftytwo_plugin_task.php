<?php
/**
 * Plugin Name:       Fiftytwo Plugin
 * Plugin URI:        https://fiftytwo.com
 * Description:       Plugin Development Task
 * Version:           0.1
 * Requires at least: 5.2
 * Requires PHP:      7.4
 * Author:            al-zami rahman
 * Author URI:        https://author.example.com/
 * License:           GPL
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       fiftytwo_plugin
 * plugin
 * Domain Path:       
 */

if(!defined('ABSPATH')){
     exit;
}

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


global $fiftytwo_plugin_version;
$fiftytwo_plugin_version = '1.0';

function fiftytwo_database_install() {
	global $wpdb;
	global $fiftytwo_plugin_version;

	$table_name = $wpdb->prefix . 'fiftytwo_plugin_currency_data';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id int NOT NULL AUTO_INCREMENT,
		name varchar(50) NOT NULL,
		currency_code varchar(50) NOT NULL,
		rate decimal(18,4) NOT NULL,
		createdOnUTC datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		updatedOnUTC datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );



	dbDelta( $sql );


    // add fiftytwo_plugin_version as an option to be used later
	add_option( 'fiftytwo_plugin_version', $fiftytwo_plugin_version );
}

//update_option('our_first_field','xyz');
require_once ( ABSPATH . 'wp-includes/option.php' ); 

require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/settings_menu.php');
require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/api_route.php');

require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/trac_currency_rate.php' );
require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/options.php' ); 
require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/fiftytwo_shortcodes.php' );
require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/second_shortcode.php');
require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/currency_data.php');
require_once ( ABSPATH . 'wp-content/plugins/fiftytwo_plugin_task/widgets/widget_for_shortcode.php');

//craete the database on plugin installation
register_activation_hook( __FILE__, 'fiftytwo_database_install' );
//register_activation_hook( __FILE__, 'fiftytwo_plugin_install_data' );


//rest api initialization
$api = new Sample_API();
$api->add_routes();
add_action('rest_api_init', function () {
	register_rest_route( 'fiftytwoplugin/v1', 'getall',array(
				  'methods'  => 'GET',
				  'callback' => 'getall_func'
		));
});

function getall_func() {
	//$arr = array('hello'=>'world');
	//$response = new WP_REST_Response($arr);
	$response = file_get_contents('https://api.exchangeratesapi.io/latest');
	$decoded_data = json_decode($response);
	$rate = $decoded_data->rates;
	//to check if cron job is working 

	global $wpdb; // this is how you get access to the database
    
    $table = $wpdb->prefix.'fiftytwo_plugin_currency_data';

	foreach($rate as $key=>$value){
		
		$currency_full_name = Currency::getCurrencyFullNameByCode($key);
		$creation_date = new \DateTime("now", new \DateTimeZone("UTC"));
		$data = array(
			'name' => $currency_full_name, 
			'currency_code' => $key,
			'rate'=> $value,
			'createdOnUTC' => $creation_date->format('Y-m-d H:m:s')
		);
		
		$format = array('%s','%s','%d');
		$wpdb->insert($table,$data,$format);
	}

    //$response->set_status(200);
	return json_decode($response);
}

add_filter( 'widget_text', 'do_shortcode', 11 );
?>
