<?php

function create_currency_rate_tracking_post_type() {
     
    // necessary labels for admin panel
    $labels = array( 
        'name' => __( 'Currencies' , 'fiftytwo_plugin' ),
        'singular_name' => __( 'Currency' , 'fiftytwo_plugin' ),
        'add_new' => __( 'New Currency Tracker' , 'fiftytwo_plugin' ),
        'add_new_item' => __( 'Add New Currency Tracker' , 'fiftytwo_plugin' ),
        'edit'=>__("Edit Currency Tracker","fiftytwo_plugin"),
        'edit_item' => __( 'Edit Currency Tracker' , 'fiftytwo_plugin' ),
        'new_item' => __( 'New Currency Tracker' , 'fiftytwo_plugin' ),
        'view_item' => __( 'View Currency Tracker' , 'fiftytwo_plugin' ),
        'search_items' => __( 'Search Currency Tracker' , 'fiftytwo_plugin' ),
        'not_found' =>  __( 'No Currency Tracker Found' , 'fiftytwo_plugin' ),
        'not_found_in_trash' => __( 'No Currency Tracker found in Trash' , 'fiftytwo_plugin' ),
    );
    
    $args = array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'publicly_queryable' => true,
        'supports' => array(
            'title',
            'editor', 
            'excerpt', 
            'custom-fields', 
            'thumbnail',
            'comments',
            'page-attributes'
        ),
        'rewrite'   => array( 'slug' => 'Currency Tracker' ),
        'show_in_rest' => true
        
 
    );
    register_post_type( 'currency_tracker', $args );
         
}

add_action( 'init', 'create_currency_rate_tracking_post_type' );


/*function delete_post_type(){
    unregister_post_type( 'currency_tracker' );
}
add_action('init','delete_post_type');*/


	
function custom_menu() { 

  add_options_page ('Custom Plugin Page', 'Custom Plugin Menu', 'manage_options', 'menu_slug', 'plugin_options_page');


 
    add_menu_page( 
        'Page Title', 
        'Menu Title', 
        'edit_posts', 
        'menu_slug', 
        'page_callback_function', 
        'dashicons-media-spreadsheet' 
   
       );

       add_submenu_page( 'menu_slug',
       "Submenu Page",
       "Now + Manually",
       'edit_posts',
       'menu_slug_add',
       'submenu_func'
    );


    // Add the menu item and page
    $page_title = '52Plugin Settings Page';
    $menu_title = '52Plugin settings';
    $capability = 'manage_options';
    $slug = 'smashing_fields';
    $callback = 'plugin_settings_page_content';
    $icon = 'dashicons-admin-plugins';
    $position = 100;

    add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
  }
  add_action( 'admin_init', 'setup_sections' ); 
  add_action( 'admin_init',  'setup_fields' );
//   add_action('init',function () {
//     $url = ABSPATH."wp-content/plugins/fiftytwo_plugin_task/resources";
//     $obj = new Test_52Plugin_Settings($url);
//   });
function setup_sections() {
    register_setting( 'smashing_fields', 'our_first_field' );

    add_settings_section( 'our_first_section', 'First Section Title', 'section_callback' , 'smashing_fields' );
    add_settings_section( 'our_second_section', 'Second Section Title',  'section_callback' , 'smashing_fields' );
    add_settings_section( 'our_third_section', 'Third Section Title','section_callback' , 'smashing_fields' );
}

function field_callback( $arguments ) {
    echo '<input name="our_first_field" id="our_first_field" type="text" value="' . get_option( 'our_first_field' ) . '" />';
}

function setup_fields() {
    add_settings_field( 'our_first_field', 'First Name', 'field_callback' , 'smashing_fields', 'our_first_section' );
}

function section_callback( $arguments ) {
    switch( $arguments['id'] ){
        case 'our_first_section':
            echo 'This is the first description here!';
            //field_callback();
            break;
        case 'our_second_section':
            echo 'This one is number two';
            break;
        case 'our_third_section':
            echo 'Third time is the charm!';
            break;
    }
}

function plugin_settings_page_content() {
?>
    <div class="wrap">
        <h2>52Plugin Settings Page</h2>
        <form method="post" action='options.php'>
            <?php
                settings_fields( 'smashing_fields' );
                do_settings_sections( 'smashing_fields' );
                submit_button();
            ?>
        </form>
    </div> 
<?php
}

function plugin_options_page(){
    echo 'hello option';
}

function page_callback_function() {
    echo 'hello world';
}

function submenu_func(){
    $url = ABSPATH."wp-content/plugins/fiftytwo_plugin_task/submission.php";
    echo $url;
    echo file_get_contents(__DIR__.'/my.php');
}
$url = plugins_url("fiftytwo_plugin_task/resources/js/admin.js");
$obj = new Test_52Plugin_Settings($url);

//register rest api

add_action('admin_menu', 'custom_menu');

?>
<?php
add_action( 'admin_footer', 'my_action_javascript' ); // Write our JS below here

function my_action_javascript() { ?>
<script>
    //let btn = document.getElementById('submit_button')
    function ajax () {

        var formData = new FormData(); 
        formData.append('action','my_action')
        formData.append(document.getElementById('currency_full_name').name,document.getElementById('currency_full_name').value)
        formData.append(document.getElementById('currency_code').name,document.getElementById('currency_code').value)
        formData.append(document.getElementById('currency_rate').name,document.getElementById('currency_rate').value)

        let ajax = new XMLHttpRequest()
        let url = ajaxurl
        console.log(url)
        ajax.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);
            };
        };
        ajax.open('POST', url, true);
        //ajax.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
        ajax.send(formData);
    }
</script>

<?php 
}

add_action( 'wp_ajax_my_action', 'my_action' );
add_action( 'wp_ajax_nopriv_my_action', 'my_action' );

function my_action() {
	global $wpdb; // this is how you get access to the database
    //print_r($_POST);
    $table = $wpdb->prefix.'fiftytwo_plugin_currency_data';
    $creation_date = new \DateTime("now", new \DateTimeZone("UTC"));
    $data = array(
        'name' => $_POST['currency_full_name'], 
        'currency_code' => $_POST['currency_code'],
        'rate'=>$_POST['currency_rate'],
        'createdOnUTC' => $creation_date->format('Y-m-d H:m:s')
    );
    
    $format = array('%s','%s','%d');
    $wpdb->insert($table,$data,$format);
}
?>