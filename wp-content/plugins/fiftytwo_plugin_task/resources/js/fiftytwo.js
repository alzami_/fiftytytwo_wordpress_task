

function filterTable(){
    
        
        var formData = new FormData(); 
        formData.append('action','shortcode_ajax_action')
        //formData.append('security',MyAjax.security)
        formData.append('filter_currency_name',document.getElementById('filter_currency_name').value)
        formData.append('filter_from_date',document.getElementById('filter_from_date').value)
        formData.append('filter_to_date',document.getElementById('filter_to_date').value)


        let ajax = new XMLHttpRequest()
        let url = MyAjax.ajaxurl
        
        ajax.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);
                deleteTableRows(JSON.parse(this.responseText));
            };
        };
        ajax.open('POST', url, true);
        ajax.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
        ajax.send(urlencodeFormData(formData));
}


function urlencodeFormData(fd){
    var params = new URLSearchParams();
    for(var pair of fd.entries()){
        typeof pair[1]=='string' && params.append(pair[0], pair[1]);
    }
    return params.toString();
}

function deleteTableRows (response) {
    let table = document.getElementById("currency_exchange_rate_table");
    var new_tbody = document.createElement('tbody');
    let old_tbody = table.tBodies[0]

    for (let i = 0; i < response.length; i++){
        var tr = document.createElement('tr');   

        for( const [key,val] of Object.entries(response[i])){
            var td = document.createElement('td');
            var text = document.createTextNode(val);
            td.appendChild(text);
            tr.appendChild(td);
        }
    
        new_tbody.appendChild(tr);
    }

    old_tbody.parentNode.replaceChild(new_tbody, old_tbody)
}

function sortTable() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("currency_exchange_rate_table");
    let sortCriterion = document.getElementById('fiftytwo_sort_criterion').value
    let sortOrder = document.getElementById('fiftytwo_sort_order').value
    //console.log(sortCriterion,sortOrder);
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("TD")[sortCriterion];
        y = rows[i + 1].getElementsByTagName("TD")[sortCriterion];
        
        // Check if the two rows should switch place:

        if(Number.isNaN(Number(x.innerHTML)) !== true) {
            if(sortOrder === "asc") {

                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                  }
            }else if (sortOrder === "desc") {
                if (Number(x.innerHTML) < Number(y.innerHTML)) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                  }
            }
        }else {
            if(sortOrder === "asc") {

                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                  }
            }else if (sortOrder === "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                  }
            }

        }


      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }
  }

  window.onload = function () {
   // console.log(document.getElementsByClassName('entry-content')[0])
    document.getElementsByClassName('entry-content')[0].classList.remove('entry-content');
    let currency_select_list = document.getElementById('filter_currency_name');
    let options = JSON.parse(MyAjax.all_currencies)
    for (let i=0; i<options.length; i++){
        currency_select_list.options[i] = new Option(options[i],options[i])
    }
    //console.log(JSON.parse(MyAjax.all_currencies))
  }

  function paginate(pageNumber) {
    var formData = new FormData(); 
    formData.append('action','shortcode_ajax_action')
    formData.append('pageNumber',pageNumber)

    let ajax = new XMLHttpRequest()
    let url = MyAjax.ajaxurl
    
    ajax.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            //console.log(this.responseText);
            deleteTableRows(JSON.parse(this.responseText));
        };
    };
    ajax.open('POST', url, true);
    //ajax.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
    ajax.send(formData);
  }