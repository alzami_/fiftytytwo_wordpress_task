<?php


function shortcode_handler_function( $atts, $content, $tag ){

    extract(shortcode_atts(array(
        'currency_code' => 'ALL',
        'limit' => 10
     ), $atts));

     wp_enqueue_script('fiftytwo_shortcode_js');
     wp_localize_script( 'fiftytwo_shortcode_js', 'MyAjax', array(
         // URL to wp-admin/admin-ajax.php to process the request
         'ajaxurl' => admin_url( 'admin-ajax.php' ),
         'all_currencies' => json_encode(Currency::selectAllCurrencyName()),
         'limit' => $limit
         // generate a nonce with a unique ID "myajax-post-comment-nonce"
         // so that you can check it later when an AJAX request is sent
     ));

    global $wpdb;
    echo '<div style="width:100%;">';
    echo '<div style="display:flex;flex-direction:row">';
    echo '<form>';
    echo '<h4 style="color:brown;">Filter Options</h4><hr><br>';
    echo '<label>Currency </label><select id="filter_currency_name">';
    echo '<option value="">---Select Currency---</option>';
    echo '</select>';
    echo '<label>From</label><input type="text" id="filter_from_date" placeholder="from-date" value="">';
    echo '<label>To</label><input type="text" placeholder="to-date" id="filter_to_date" value="">';
    echo '<input type="button" style="background:white;color:black;" value="filter table" id="filter_btn" onclick="filterTable();" >';
    echo '<br><br><hr><br>';
    echo '<h4 style="color:green;">Currency Exchange Rate Table</h4><hr><br>';
    $table = $wpdb->prefix.'fiftytwo_plugin_currency_data';

    if($currency_code == 'ALL') {
        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$table} limit %d ",$limit ) );
        $totalRowCount = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$table}"));
    }else {
        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$table} where `currency_code` = %s limit %d ",$currency_code, $limit) );
        $totalRowCount = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$table} where `currency_code` = %s",$currency_code));
    
    }
    
    $headers = $results[0];
    echo '<div style="display:flex;flex-direction:row;">';
    echo '<label>Sorting Options : </label>';
    echo '<select id="fiftytwo_sort_criterion" onchange="sortTable();">';
    echo '<option value="0">Id</option>';
    echo '<option value="1">Currency Name</option>';
    echo '<option value="2">Currency Code</option>';
    echo '<option value="3">Currency Rate</option>';
    echo '<option value="4">Creation Date</option>';
    echo '</select>';
    echo '<select id="fiftytwo_sort_order" onchange="sortTable();">';
    echo '<option value="asc">Ascending</option>';
    echo '<option value="desc">Desending</option>';

    echo '</select>';
    echo '</div><br>';
    echo '<table id="currency_exchange_rate_table">';
    echo '<thead>';
    foreach ($headers as $key=>$value) {
        echo '<th>'.$key.'</th>';
    }
    echo '</thead>';
    echo '<tbody>';
    foreach($results as $key=>$value){
        echo '<tr>';
        foreach($value as $key2=>$value2){
            echo '<td>'.$value2.'</td>';
        }
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
    echo '<div style="display:flex; flex-direction:row;">';
    $totalPageRequired =  ceil($totalRowCount/$limit);
    for($i=0;$i<$totalPageRequired;$i++) {
        echo '<input type="button" value="'.$i.'" onclick="paginate('.($i+1).')" />';
    }
    echo '</div>';
    echo '</div>';
 }

 add_action('init', 'shortcode_init');

 function shortcode_init(){
    add_shortcode( 'fiftytwo_shortcode', 'shortcode_handler_function' );
    add_shortcode( 'fiftytwo_shortcode_two', 'shortcode_handler_function_two' );

 }

 function shortcode_handler_function_two() {
     echo '<h1>Hello WOrld</h1>';
 }
 // First register resources with init 
function fiftytwo_shortcode_js() {
	wp_register_script("fiftytwo_shortcode_js", plugins_url("fiftytwo_plugin_task/resources/js/fiftytwo.js"));

    // wp_enqueue_script('fiftytwo_shortcode_js');
    // wp_localize_script( 'fiftytwo_shortcode_js', 'MyAjax', array(
    //     // URL to wp-admin/admin-ajax.php to process the request
    //     'ajaxurl' => admin_url( 'admin-ajax.php' ),
    //     'all_currencies' => json_encode(Currency::selectAllCurrencyName())
    //     // generate a nonce with a unique ID "myajax-post-comment-nonce"
    //     // so that you can check it later when an AJAX request is sent
    // ));
}

add_action( 'init', 'fiftytwo_shortcode_js' );


// The function that handles the AJAX request

function shortcode_ajax_action() {
    //check_ajax_referer( 'my-special-string', 'security' );

    global $wpdb;
    

    $table = $wpdb->prefix.'fiftytwo_plugin_currency_data';
    $currency_code = $_POST['filter_currency_name'];
    //echo $currency_code;
    $from_date = isset($_POST['filter_from_date']) && !empty($_POST['filter_from_date']) ? $_POST['filter_from_date'] : '1970-01-01';
    $to_date = isset($_POST['filter_to_date']) && !empty($_POST['filter_to_date'])? $_POST['filter_to_date'] : '2025-01-01';


    if($currency_code == 'ALL') {
        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$table} where  DATE(`createdOnUTC`) between %s and %s limit 5", $from_date,$to_date ) );

    }else {
        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$table} where `currency_code` = %s and DATE(`createdOnUTC`) between %s and %s limit 5", $currency_code, $from_date,$to_date ) );

    }
    echo json_encode($results);
    //return $results;
    exit();
  }
add_action( 'wp_ajax_shortcode_ajax_action', 'shortcode_ajax_action' );
add_action( 'wp_ajax_nopriv_shortcode_ajax_action', 'shortcode_ajax_action' );


// pagination code here

function pagination_ajax_action() {
    
    global $wpdb;

    
    $table = $wpdb->prefix.'fiftytwo_plugin_currency_data';
    $currency_code = $_POST['filter_currency_name'];
    $page_number = $_POST['pageNumber'];
    $from_date = isset($_POST['filter_from_date']) && !empty($_POST['filter_from_date']) ? $_POST['filter_from_date'] : '1970-01-01';
    $to_date = isset($_POST['filter_to_date']) && !empty($_POST['filter_to_date'])? $_POST['filter_to_date'] : '2025-01-01';


    if($currency_code == 'ALL') {
        
        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$table} where  DATE(`createdOnUTC`) between %s and %s limit %d", $from_date,$to_date,$limit ) );

    }else {

        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$table} where `currency_code` = %s and DATE(`createdOnUTC`) between %s and %s limit 5", $currency_code, $from_date,$to_date ) );

    }
    echo json_encode($results);
    //return $results;
    die();
    
}


add_action( 'wp_ajax_pagination_ajax_action', 'pagination_ajax_action' );
add_action( 'wp_ajax_nopriv_pagination_ajax_action', 'pagination_ajax_action' );
?>