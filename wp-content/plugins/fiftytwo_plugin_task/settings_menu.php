<?php
class Test_52Plugin_Settings {
	/**
	 * Menu slug
	 *
	 * @var string
	 */
	protected $slug = 'fiftytwo_plugin_settings';
	/**
	 * URL for assets
	 *
	 * @var string
	 */
	protected $assets_url;
	/**
	 * Apex_Menu constructor.
	 *
	 * @param string $assets_url URL for assets
	 */
	public function __construct( $assets_url ) {
		$this->assets_url = $assets_url;
		add_action( 'admin_menu', array( $this, 'add_page' ) );
        add_action( 'admin_init', array($this,'setup_sections') ); 
        add_action( 'admin_init', array($this,'setup_fields') );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_assets' ) );
	}

    public function field_callback( $arguments ) {
        echo '<input name="oop_field" id="oop_field" type="text" value="' . get_option( 'oop_field' ) . '" />';
    }

    public function setup_fields() {
        add_settings_field( 'oop_field', 'oop first Name', array($this,'field_callback') , $this->slug, 'our_first_section' );
    }

	/**
	 * Add CF Popup submenu page
	 *
	 * @since 0.0.3
	 *
	 * @uses "admin_menu"
	 */
	public function add_page(){
		add_menu_page(
            __( 'Another Page', 'text-domain' ),
			__( 'Another Page', 'text-domain' ),
		    'manage_options',
			$this->slug,
			array( $this, 'render_admin' ) );
	}

    public function setup_sections() {
        register_setting( $this->slug, 'oop_field' );
    
        add_settings_section( 'our_first_section', 'First Section Title', array($this,'section_callback') , $this->slug );
        add_settings_section( 'our_second_section', 'Second Section Title',  array($this,'section_callback') , $this->slug );
        add_settings_section( 'our_third_section', 'Third Section Title', array($this,'section_callback') , $this->slug );
    }

    public function section_callback( $arguments ) {
        switch( $arguments['id'] ){
            case 'our_first_section':
                echo 'This is the first description here!';
                break;
            case 'our_second_section':
                echo 'This one is number two';
                break;
            case 'our_third_section':
                echo 'Third time is the charm!';
                break;
        }
    }
	/**
	 * Register CSS and JS for page
	 *
	 * @uses "admin_enqueue_scripts" action
	 */
	public function register_assets()
	{
		wp_register_script( $this->slug, $this->assets_url );
		//wp_register_style( $this->slug, $this->assets_url . '/css/admin.css' );
		wp_localize_script( $this->slug, 'SET_OPTION_AJAX', array(
			'strings' => array(
				'saved' => __( 'Settings Saved', 'text-domain' ),
				'error' => __( 'Error', 'text-domain' )
			),
			'api'     => array(
				'url'   => esc_url_raw( rest_url( 'sampleapi/v1/dummy_settings_post' ) ),
				'nonce' => wp_create_nonce( 'wp_rest' )
			)
		) );
	}
	/**
	 * Enqueue CSS and JS for page
	 */
	public function enqueue_assets(){
		if( ! wp_script_is( $this->slug, 'registered' ) ){
			$this->register_assets();
		}
		wp_enqueue_script( $this->slug );
		wp_enqueue_style( $this->slug );
	}
	/**
	 * Render plugin admin page
	 */
	public function render_admin(){
		$this->enqueue_assets();
        echo '<div class="wrap">';
        echo '<h2>Another Plugin Settings Page</h2>';
        echo "<form action='options.php' method='post'>";
        echo settings_fields( $this->slug );
        echo do_settings_sections( $this->slug );
        echo '<input type="button" value="submit form" onclick="setting_update_ajax();" />';
        echo "</form>";
        echo '</div>';
	}
}

?>