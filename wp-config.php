<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fiftytwo_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'shR ]Aka6(bx,/+)(q@F3k9:IL(/CdSA64g=hYGU]j/Gi1xj-YWKE[]Zz4Z,Iw/[' );
define( 'SECURE_AUTH_KEY',  '-m8O~1oIxL8GA<nmSI.a2JiI*t39Iy<!( Z%)dT2)sv8x`k1;lc(};17N+n!oDbi' );
define( 'LOGGED_IN_KEY',    'rP;?jH~x|(s2mt&$h:6i!o]w&eEN`]@6Z$LPOlPRN7D1LD$Hl`npY$;Q?vgJ#Zi ' );
define( 'NONCE_KEY',        'qawI?zahCuZu^5;n[m&$x7#2Oi]6|.P9c_H,FWn?vF>_v?S9!hHl-X5~qWQjl}-&' );
define( 'AUTH_SALT',        '2I8$3ct=v_8%chf%|J5hBgBBM@7%~|YN*OR@]srr[!)}pg:<Nx!xeV^RV}F#nlrl' );
define( 'SECURE_AUTH_SALT', '1>B9?U`Uny NU-i) e8E<BX6~c-Dg:n,i#[//K>c,$;#6DR0gC7yKAn{i74_g(J~' );
define( 'LOGGED_IN_SALT',   'K}V6{;ktN9l~DmTw~n?a44#L`5s~_y_c5lam.^>Wp&PUVI4*fo9i;Mk$?UAI >rM' );
define( 'NONCE_SALT',       '*i7B#uz)VNLDDnLS}UwL?>)j~SqMM,Z@[,S@IauuhJc1R&EKb3df+nn1N*ORl[.%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
